output "vm_info" {
  value = {
    dev01 = {
      name = module.vm-dev.hostname
      ip   = module.vm-dev.external_ip
    }
  }
}