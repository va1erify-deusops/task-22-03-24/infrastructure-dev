module "network-dev" {
  source = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-vpc-network/yandex-cloud/0.2.0"
  name   = "network-dev"
}

module "dns-zone" {
  source      = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-dns-zone/yandex-cloud/0.1.0"
  folder_id   = var.folder_id
  name        = "todolist-public-zone"
  description = ""
  depends_on  = [module.network-dev]
  labels = {
    label1 = "todolist"
  }
  zone                = "va1erify.ru."
  public              = true
  private_networks    = [module.network-dev.id]
  deletion_protection = false
}

module "dns-recordset-dev" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-dns-recordset/yandex-cloud/0.2.0"
  depends_on = [module.dns-zone, module.vm-dev]
  zone_id    = module.dns-zone.id
  name       = "*.dev.va1erify.ru."
  type       = "A"
  ttl        = "200"
  data       = [module.vm-dev.external_ip]
}

module "dns-recordset-wildcard-dev" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-dns-recordset/yandex-cloud/0.2.0"
  depends_on = [module.dns-zone, module.vm-dev]
  zone_id    = module.dns-zone.id
  name       = "dev.va1erify.ru."
  type       = "A"
  ttl        = "200"
  data       = [module.vm-dev.external_ip]
}

module "dns-recordset-traefik-dev" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-dns-recordset/yandex-cloud/0.2.0"
  depends_on = [module.dns-zone, module.vm-dev]
  zone_id    = module.dns-zone.id
  name       = "traefik.dev.va1erify.ru."
  type       = "A"
  ttl        = "200"
  data       = [module.vm-dev.external_ip]
}

module "dns-recordset-portainer-dev" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-dns-recordset/yandex-cloud/0.2.0"
  depends_on = [module.dns-zone, module.vm-dev]
  zone_id    = module.dns-zone.id
  name       = "portainer.dev.va1erify.ru."
  type       = "A"
  ttl        = "200"
  data       = [module.vm-dev.external_ip]
}

module "subnetwork-dev" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/vps-subnet/yandex-cloud/0.2.0"
  depends_on = [module.network-dev]
  id         = module.network-dev.id
  zone       = var.zone
  cidr_v4    = "10.10.10.0/24"
  name       = "subnetwork-dev"
}

module "vm-dev" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.1.0"
  depends_on           = [module.subnetwork-dev]
  subnetwork_id        = module.subnetwork-dev.id
  name                 = "dev01"
  internal_ip_address  = "10.10.10.10"
  hostname             = "dev01"
  platform             = "standard-v2"
  ram                  = "2"
  cpu                  = "2"
  core_fraction        = "100"
  boot_disk_image_id   = "fd8t849k1aoosejtcicj"
  boot_disk_size       = "15"
  boot_disk_type       = "network-hdd"
  boot_disk_block_size = "4096"
  nat                  = true
  preemptible          = false
  path_to_cloud_config = "./cloud-config"
}