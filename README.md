## Создаваемые сущности

- `module "network-dev"`: Создание сети.
- `module "subnetwork-dev"`: Создание подсети.
- `module "dns-zone"`: Создание dns зоны.
- `module "dns-recordset-dev"`: Создание dns записи для приложения.
- `module "dns-recordset-wildcard-dev"`: Создание dns записи для приложения.
- `module "dns-recordset-traefik-dev"`: Создание dns записи для traefik.
- `module "dns-recordset-portainer-dev"`: Создание dns записи для portainer.
- `module "vm-dev"`: Создание ВМ.